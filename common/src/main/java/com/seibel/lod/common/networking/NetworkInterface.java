package com.seibel.lod.common.networking;

/**
 * @author Ran
 */
public interface NetworkInterface {
    void register_Client();
    void register_Server();
}
